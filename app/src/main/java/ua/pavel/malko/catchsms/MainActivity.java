package ua.pavel.malko.catchsms;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class MainActivity extends AppCompatActivity {
    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    private static final int SMS_PERMISSION_REQUEST = 357;
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy h:mm:ss");
    private static final int GOOGLE_FIT_PERMISSIONS_REQUEST_CODE = 333;
    private final IntentFilter smsIndentFilter = new IntentFilter();
    private Button btRegister, btUnRegister, btGoogleFitConnect;
    private TextView tvLog;
    final FitnessOptions fitnessOptions = FitnessOptions.builder()
            .addDataType(DataType.TYPE_WEIGHT, FitnessOptions.ACCESS_READ)
            .addDataType(DataType.TYPE_HEIGHT, FitnessOptions.ACCESS_READ)
            .build();
    private final BroadcastReceiver simsBroadcastsReciter = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(LOG_TAG, "onReceive() called with: context = [" + context + "], intent = [" + intent + "]");

            if (Telephony.Sms.Intents.SMS_RECEIVED_ACTION.equals(intent.getAction())) {
                for (SmsMessage smsMessage : Telephony.Sms.Intents.getMessagesFromIntent(intent)) {
                    appendText(smsMessage.getMessageBody());
                }
            }
        }
    };

    private void appendText(String text) {
        tvLog.setText(
                tvLog.getText().toString().concat(
                        getString(R.string.log_text,
                                dateFormat.format(new Date()),
                                text)
                )
        );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        smsIndentFilter.addAction(Telephony.Sms.Intents.SMS_RECEIVED_ACTION);
        smsIndentFilter.setPriority(999);

        btRegister = findViewById(R.id.bt_register_receiver);
        btUnRegister = findViewById(R.id.bt_un_register_receiver);
        btGoogleFitConnect = findViewById(R.id.bt_fit_connect);
        tvLog = findViewById(R.id.tv_log);

        btRegister.setOnClickListener((view) -> {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED) {
//            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
//                    Manifest.permission.RECEIVE_SMS)) {
//                // Show an explanation to the user *asynchronously* -- don't block
//                // this thread waiting for the user's response! After the user
//                // sees the explanation, try again to request the permission.
//            } else {
//                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.RECEIVE_SMS},
                        SMS_PERMISSION_REQUEST);
//            }
            } else {
                btRegister.setEnabled(false);
                btUnRegister.setEnabled(true);

                registerReceiver(simsBroadcastsReciter, smsIndentFilter);
            }
        });
        btGoogleFitConnect.setOnClickListener((view) -> {

            if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(this), fitnessOptions)) {
                Log.d(LOG_TAG, "onCreate() called with: savedInstanceState = [" + savedInstanceState + "]");
//                final Intent intent =
//                        GoogleSignIn.getClient(this, new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build())
//                                .getSignInIntent();
//                if (getPackageManager().resolveActivity(intent, 0) != null) {
//                    startActivityForResult(intent, GOOGLE_FIT_PERMISSIONS_REQUEST_CODE);
//                }
                GoogleSignIn.requestPermissions(
                        this, // your activity
                        GOOGLE_FIT_PERMISSIONS_REQUEST_CODE,
                        GoogleSignIn.getLastSignedInAccount(this),
                        fitnessOptions);

            } else {
                Log.d(LOG_TAG, "onCreate() called with: savedInstanceState = [" + savedInstanceState + "]");
                accessGoogleFit();
            }
        });
        btUnRegister.setOnClickListener((view) -> {
            btRegister.setEnabled(true);
            btUnRegister.setEnabled(false);

            unregisterReceiver(simsBroadcastsReciter);
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(LOG_TAG, "onRequestPermissionsResult() called with: requestCode = [" + requestCode + "], permissions = [" + permissions + "], grantResults = [" + grantResults + "]");
        switch (requestCode) {
            case SMS_PERMISSION_REQUEST:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    btRegister.callOnClick();
                }
                break;
            case GOOGLE_FIT_PERMISSIONS_REQUEST_CODE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    accessGoogleFit();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void accessGoogleFit() {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        final long endTime = cal.getTimeInMillis();
        cal.add(Calendar.YEAR, -10);
        final long startTime = cal.getTimeInMillis();

//        final DataReadRequest readRequest = new DataReadRequest.Builder()
//                .aggregate(DataType.TYPE_WEIGHT, DataType.AGGREGATE_WEIGHT_SUMMARY)
//                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
//                .bucketByTime(31, TimeUnit.DAYS)
//                .build();

        final DataReadRequest readHeightRequest = new DataReadRequest.Builder()
                .read(DataType.TYPE_HEIGHT)
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .setLimit(1)
                .build();
        final DataReadRequest readWeightRequest = new DataReadRequest.Builder()
                .read(DataType.TYPE_WEIGHT)
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .setLimit(1)
                .build();

        final GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        if (account != null) {
//            final GoogleSignInClient mClient = new GoogleApiClient.Builder(this)
//                    .addApi(Fitness.HISTORY_API)
//                    .addScope(new Scope(Scopes.FITNESS_BODY_READ))
//                    .build()
//                    .getClient(GoogleSignIn.getLastSignedInAccount(this));
            Fitness.getHistoryClient(this, GoogleSignIn.getLastSignedInAccount(this))
                    .readData(readHeightRequest)
                    .addOnSuccessListener(dataReadResponse -> {
                        Log.d(LOG_TAG, "onSuccess() called with: dataReadResponse = [" + dataReadResponse + "] "
                                + "result: [" + dataReadResponse.getStatus().getStatus() + "] "
                                + "message: " + dataReadResponse.getStatus().getStatusMessage());
//                        if (dataReadResponse.getStatus() == Status.RESULT_SUCCESS) {
                            for (DataSet dataSet : dataReadResponse.getDataSets()) {
                                Log.d(LOG_TAG, String.format("accessGoogleFit: type [%s], source [%s]",
                                        dataSet.getDataType().getName(),
                                        dataSet.getDataSource().getName()));
                                for (DataPoint point : dataSet.getDataPoints()) {
                                    Log.d(LOG_TAG, String.format("accessGoogleFit: type [%s]",
                                            point.getDataType()
                                    ));
                                        Log.d(LOG_TAG, String.format("accessGoogleFit: height %f",
                                                point.getValue(Field.FIELD_HEIGHT).asFloat()));
                                        appendText(String.format(
                                                getResources().getConfiguration().locale,
                                                "You height: %1.2f, at date %s",
                                                point.getValue(Field.FIELD_HEIGHT).asFloat(),
                                                dateFormat.format(point.getTimestamp(TimeUnit.MILLISECONDS))
                                        ));
                                }
                            }
//                        }
                    })
                    .addOnFailureListener(e -> Log.e(LOG_TAG, "onFailure()", e));
            Fitness.getHistoryClient(this, GoogleSignIn.getLastSignedInAccount(this))
                    .readData(readWeightRequest)
                    .addOnSuccessListener(dataReadResponse -> {
                        Log.d(LOG_TAG, "onSuccess() called with: dataReadResponse = [" + dataReadResponse + "] "
                                + "result: [" + dataReadResponse.getStatus().getStatus() + "] "
                                + "message: " + dataReadResponse.getStatus().getStatusMessage());
//                        if (dataReadResponse.getStatus() == Status.RESULT_SUCCESS) {
                            for (DataSet dataSet : dataReadResponse.getDataSets()) {
                                Log.d(LOG_TAG, String.format("accessGoogleFit: type [%s], source [%s]",
                                        dataSet.getDataType().getName(),
                                        dataSet.getDataSource().getName()));
                                for (DataPoint point : dataSet.getDataPoints()) {
                                    Log.d(LOG_TAG, String.format("accessGoogleFit: type [%s]",
                                            point.getDataType()
                                    ));
                                        Log.d(LOG_TAG, String.format("accessGoogleFit: weight %f",
                                                point.getValue(Field.FIELD_WEIGHT).asFloat()));
                                    appendText(String.format(
                                            getResources().getConfiguration().locale,
                                            "You weight: %1.2f, at date %s",
                                            point.getValue(Field.FIELD_WEIGHT).asFloat(),
                                            dateFormat.format(point.getTimestamp(TimeUnit.MILLISECONDS))
                                    ));
                                }
                            }
//                        }
                    })
                    .addOnFailureListener(e -> Log.e(LOG_TAG, "onFailure()", e));
        } else {
            Log.d(LOG_TAG, String.format("accessGoogleFit: GoogleSignIn.getLastSignedInAccount(this) == null"));
//            final Intent intent = GoogleSignIn.getClient(this, GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN).getSignInIntent();
//            if (getPackageManager().resolveActivity(intent, 0) != null)
//                startActivityForResult(intent, GOOGLE_FIT_PERMISSIONS_REQUEST_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Log.d(LOG_TAG, "onActivityResult() called with: requestCode = [" + requestCode + "], resultCode = [" + resultCode + "], data = [" + data + "]");
        Log.d(LOG_TAG, String.format("onActivityResult: %s",
                data.getExtras()));
        for (String key : data.getExtras().keySet())
            Log.d(LOG_TAG, String.format("onActivityResult: [%s:%s]",
                    key, data.getExtras().get(key).toString()));
        switch (requestCode) {
            case GOOGLE_FIT_PERMISSIONS_REQUEST_CODE:
                accessGoogleFit();
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
